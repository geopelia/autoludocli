﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoLudoCLI.Models
{
    class Dice
    {
        // Singleton pattern from https://csharpindepth.com/Articles/Singleton Second Method
        private static Dice _instance = null;
        private static readonly object padlock = new object();
        public static Dice Instance
        {
            get
            {
                lock(padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new Dice();
                    }
                    return _instance;
                }
            }
        }
        readonly int[] valuesAllowed = { 1, 2, 3, 4, 5, 6 };

        // With this value the player its allowed to bring a new token to the game
        readonly int specialValue = 6;

        public int Roll()
        {
            Random rnd = new Random();
            int randomIndex = rnd.Next(0, valuesAllowed.Length);
            return valuesAllowed[randomIndex];
        }

        public bool IsStartNumber(int number)
        {
            return number == specialValue;
        }

        internal bool IsMaxNumber(int playerRoll)
        {
            return playerRoll == valuesAllowed[valuesAllowed.Length - 1];
        }
        
    }
}
