﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoLudoCLI.Models
{
    public enum Color
    {
        Red,
        Green,
        Yellow,
        Blue
    }

    public readonly struct TeamColor
    {
        private const int NULL_POSITION = -100;
        

        public TeamColor(Color color)
        {
            MyColor = color;
            switch (color)
            {
                case Color.Red:
                    StartPosition = 13;
                    SecureRoadPosition = 11;                    
                    break;
                case Color.Blue:
                    StartPosition = 0;
                    SecureRoadPosition = 50;                    
                    break;
                case Color.Yellow:
                    StartPosition = 39;
                    SecureRoadPosition = 37;                    
                    break;
                case Color.Green:
                    StartPosition = 26;
                    SecureRoadPosition = 24;                    
                    break;
                default:
                    StartPosition = NULL_POSITION;
                    SecureRoadPosition = NULL_POSITION;
                    break;
            }
            

        }

        public Color MyColor { get; }
        public int  StartPosition { get; }
        public int SecureRoadPosition { get; }
    }
}
