﻿using System;
using System.Linq;
using System.Text;

namespace AutoLudoCLI.Models
{
    public class Board
    {
        private static Board instance = null;
        private static readonly object padlock = new object();

        public static Board Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Board();
                    }
                    return instance;
                }
            }
        }

        public Player[] PlayersOnBoard { get; set; }

        public const int MAX_BOARD_POSITION = 51;
        private const int NUMBER_OF_PLAYERS = 4;
        private readonly string txtBoard;

        public Board()
        {
            PlayersOnBoard = new Player[NUMBER_OF_PLAYERS];
            for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
            {
                Color color = (Color)i;
                TeamColor teamColor = new TeamColor(color);
                Player player = new Player(teamColor);
                PlayersOnBoard[i] = player;
            }
            StringBuilder stringBuilder = new StringBuilder("|");
            for (int i = 0; i <= MAX_BOARD_POSITION; i++)
            {
                stringBuilder.Append(i.ToString("D2") + "|");
            }
            stringBuilder.Append("\n");
            txtBoard = stringBuilder.ToString();
        }

        public void ResetPlayers()
        {
            Array.Clear(PlayersOnBoard, 0, NUMBER_OF_PLAYERS);
            for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
            {
                Color color = (Color)i;
                TeamColor teamColor = new TeamColor(color);
                Player player = new Player(teamColor);
                PlayersOnBoard[i] = player;
            }
        }


        public static void PrintBoardPosition(Player player)
        {
            switch (player.MyTeamColor.MyColor)
            {
                case Color.Red:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    break;
                case Color.Blue:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case Color.Yellow:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                case Color.Green:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                default:
                    break;
            }
            int atStart = 0;
            int atFinish = 0;
            StringBuilder stringBuilder = new StringBuilder();
            foreach (GameToken token in player.Tokens)
            {
                if (!token.IsAtStart && !token.IsAtFinish)
                {
                    int pos = (token.Position * 3) + 2;
                    if (pos < 0)
                    {
                        pos = 0;
                    }
                    if (token.Position == player.MyTeamColor.StartPosition)
                    {
                        stringBuilder.Append("i".PadLeft(pos) + "\n");
                    }
                    else if (token.Position == GameToken.OUT_OF_BOUND_POSITION && token.SafeZonePosition >= 0)
                    {
                        stringBuilder.Append("SAFE id " + token.Id + " and pos: " + token.SafeZonePosition + "\n");
                    }
                    else
                    {
                        stringBuilder.Append("x".PadLeft(pos) + "\n");
                    }

                }
                if (token.IsAtStart)
                {
                    atStart++;
                }
                else if (token.IsAtFinish)
                {
                    atFinish++;
                }

            }
            stringBuilder.Append("---------\n");

            stringBuilder.Append("At start:  " + new string('x', atStart) + " At Finish: " + new string('x', atFinish) + "\n");
            Console.WriteLine(stringBuilder.ToString());
            Console.ResetColor();


        }

        public void PrintBoardTitle(int steps)
        {
            Console.WriteLine("     En el paso {0}", steps);
            Console.Write(txtBoard);
        }

        public void PrintWinner(int steps)
        {
            Player winner = null;
            foreach (Player player in PlayersOnBoard)
            {
                if (player.HasWon())
                {
                    winner = player;
                    break;
                }
            }
            if (winner == null)
            {
                throw new Exception("Debio haber un ganador a esta altura");
            }
            Console.WriteLine("\n\nHa ganadado el jugador {0} en {1} pasos!!", winner.MyTeamColor.MyColor, steps);
            Console.Beep();

        }

        public bool IsPositionOccupiedByAnotherPlayerToken(int position, TeamColor
            currentTeam)
        {
            if (PlayersOnBoard.Length == 0)
            {
                throw new Exception("No hay jugadores en la clase Board");
            }
            foreach (Player item in PlayersOnBoard)
            {
                if (!item.MyTeamColor.Equals(currentTeam))
                {
                    foreach (GameToken token in item.Tokens)
                    {
                        if (!token.IsAtFinish && !token.IsAtStart
                            && token.Position != GameToken.OUT_OF_BOUND_POSITION
                            && token.Position != item.MyTeamColor.StartPosition
                            && !item.IsTokenInBlockade(token))
                        {
                            if (position == token.Position)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public void StealPosition(int tokenPosition, TeamColor myTeamColor)
        {
            if (PlayersOnBoard.Length == 0)
            {
                throw new Exception("No hay jugadores en la clase Board");
            }
            foreach (Player item in PlayersOnBoard)
            {
                if (!item.MyTeamColor.Equals(myTeamColor))
                {
                    foreach (GameToken token in item.Tokens)
                    {
                        if (!token.IsAtFinish && !token.IsAtStart
                            && token.Position != GameToken.OUT_OF_BOUND_POSITION)
                        {
                            if (tokenPosition == token.Position)
                            {
                                item.SendTokenToStart(token);
                                break;
                            }
                        }
                    }
                }
            }
        }

        public void DeterminePlayersOrder()
        {
            int maxRoll = 0;
            Player firsPlayer = null;
            Player[] tempPlayers = new Player[NUMBER_OF_PLAYERS];
            foreach (Player player in PlayersOnBoard)
            {
                int playerRoll = player.RollDice();
                if (playerRoll > maxRoll)
                {
                    maxRoll = playerRoll;
                    firsPlayer = player;
                }
                if (Dice.Instance.IsMaxNumber(maxRoll))
                {
                    break;
                }
                Console.WriteLine(" color: {0} y lanzada {1} ", player.MyTeamColor.MyColor, playerRoll);
            }
            int j = 0;
            tempPlayers[j++] = firsPlayer;

            while (j < NUMBER_OF_PLAYERS)
            {
                int nextColor = ((int)firsPlayer.MyTeamColor.MyColor) + 1;
                if (nextColor == NUMBER_OF_PLAYERS)
                {
                    nextColor = 0;
                }
                firsPlayer = PlayersOnBoard.Where(item => item.MyTeamColor.MyColor == (Color)nextColor).Select(item => item).Single();
                tempPlayers[j++] = firsPlayer;
            }
            PlayersOnBoard = tempPlayers;
            Console.WriteLine("gente ordenada: ");
            foreach (var item in tempPlayers)
            {
                Console.WriteLine(item.MyTeamColor.MyColor);
            }
        }

        public bool IsThereAWinner()
        {
            foreach (Player player in PlayersOnBoard)
            {
                if (player.HasWon())
                {
                    player.DebugPlayer();
                    return true;
                }
            }
            return false;
        }

        public bool IsThereABlockade(int initialPosition, int endPosition, TeamColor myTeamColor)
        {
            if (PlayersOnBoard.Length == 0)
            {
                throw new Exception("No hay jugadores en la clase Board");
            }
            foreach (Player item in PlayersOnBoard)
            {
                if (!item.MyTeamColor.Equals(myTeamColor))
                {
                    int i = initialPosition;
                    int j = (endPosition + 1);
                    if (j > Board.MAX_BOARD_POSITION)
                    {
                        j -= (Board.MAX_BOARD_POSITION + 1);
                    }
                    while (i != j)
                    {
                        if (item.IsThereABlockade(i, false))
                        {
                            return true;
                        }
                        i++;
                        if (i > Board.MAX_BOARD_POSITION)
                        {
                            i -= (Board.MAX_BOARD_POSITION + 1);
                        }

                    }

                }
            }
            return false;
        }
        public bool IsThereABlockadeInSafeZone(TeamColor myTeamColor, int position)
        {
            if (PlayersOnBoard.Length == 0)
            {
                throw new Exception("No hay jugadores en la clase Board");
            }
            foreach (Player item in PlayersOnBoard)
            {
                if (!item.MyTeamColor.Equals(myTeamColor))
                {
                    if (item.IsThereABlockade(position, true))
                    {
                        return true;
                    }


                }
            }
            return false;
        }
    }
}
