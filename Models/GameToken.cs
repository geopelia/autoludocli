﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoLudoCLI.Models
{
    public class GameToken
    {
        public const int OUT_OF_BOUND_POSITION = -100;
        public const int WINNING_SAFE_POSITION = 6;

        public GameToken(Color myColor, int id)
        {
            Position = OUT_OF_BOUND_POSITION;
            SafeZonePosition = OUT_OF_BOUND_POSITION;
            IsAtStart = true;
            IsAtFinish = false;
            AssignedColor = myColor;
            Steps = 0;
            this.Id = id;
        }

        public int Position { get; set; }
        public int SafeZonePosition { get; set; }
        public bool IsAtStart { get; set; }
        public bool IsAtFinish { get; set; }
        public Color AssignedColor { get; }
        public int Steps { get; set; }

        public int Id { get; }

        public void DebugTokenPos()
        {

            if (!IsAtStart && !IsAtFinish)
            {
                if (Position != GameToken.OUT_OF_BOUND_POSITION)
                {
                    Console.WriteLine(" token de color {0} en posicion {1}", AssignedColor, Position);
                }
                if (SafeZonePosition != GameToken.OUT_OF_BOUND_POSITION)
                {
                    Console.WriteLine(" token de color {0} en posicion segura {1}", AssignedColor, SafeZonePosition);
                }
            }
            if (IsAtStart)
            {
                Console.WriteLine(" token de color {0} al inicio", AssignedColor);
            }
            if (IsAtFinish)
            {
                Console.WriteLine(" token de color {0} en la meta", AssignedColor);
            }

        }

        public override string ToString()
        {
            return "Color: " + AssignedColor + " Id: " + Id;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || !(obj is GameToken))
                return false;
            else
                return Id == ((GameToken)obj).Id && AssignedColor == ((GameToken)obj).AssignedColor;
        }


        public override int GetHashCode()
        {
            String number = ((int)AssignedColor).ToString() + Id.ToString();
            return Int16.Parse(number);
        }

        public void MoveInNormalTiles(int roll, TeamColor MyTeamColor, int newPosition)
        {
            int oldPosition = Position;
            if (Steps > Board.MAX_BOARD_POSITION - 1)
            {
                //Console.WriteLine("color {2} - posicion sin calcular {0} , posicion segura {1} - roll {3} y pasos {4}", newPosition, 
                //    (newPosition - MyTeamColor.SecureRoadPosition), MyTeamColor.MyColor, roll, Tokens[firstPlayerIndex].Steps);
                if (AssignedColor == Color.Blue)
                {
                    if (newPosition < MyTeamColor.SecureRoadPosition)
                    {
                        MoveInSafeZone(roll, MyTeamColor, newPosition, oldPosition);
                    }
                }
                else
                {
                    if (newPosition > MyTeamColor.SecureRoadPosition)
                    {
                        MoveInSafeZone(roll, MyTeamColor, newPosition, oldPosition);
                    }
                }

            }
            else
            {
                Position = newPosition;
                if (Board.Instance.IsPositionOccupiedByAnotherPlayerToken(newPosition, MyTeamColor))
                {
                    Board.Instance.StealPosition(newPosition, MyTeamColor);
                }
                if (Position > MyTeamColor.SecureRoadPosition && Steps > 50)
                {
                    Console.WriteLine("color {2} - posicion sin calcular {0} , posicion segura {1} - roll {3} , posicion vieja {5} y pasos {4}",
                        newPosition, (newPosition - MyTeamColor.SecureRoadPosition), MyTeamColor.MyColor, roll,
                        Steps, oldPosition);
                    throw new Exception("Nos pasamos de la zona segura!!");
                }
            }
        }

        private void MoveInSafeZone(int roll, TeamColor MyTeamColor, int newPosition, int oldPosition)
        {
            Position = GameToken.OUT_OF_BOUND_POSITION;
            if (AssignedColor != Color.Blue)
            {
                SafeZonePosition = newPosition - MyTeamColor.SecureRoadPosition;
            }
            else
            {
                SafeZonePosition = (oldPosition + roll) - MyTeamColor.SecureRoadPosition;
            }

            if (SafeZonePosition > GameToken.WINNING_SAFE_POSITION)
            {
                Console.WriteLine("color {2} - posicion sin calcular {0} , posicion segura {1} - roll {3} , posicion vieja {5} y pasos {4}",
                    newPosition, (newPosition - MyTeamColor.SecureRoadPosition), MyTeamColor.MyColor, roll,
                    Steps, oldPosition);
                //Tokens[firstPlayerIndex].DebugTokenPos();
                throw new Exception("Nos pasamos de posiciones!!");
            }
            if (SafeZonePosition == GameToken.WINNING_SAFE_POSITION)
            {
                SafeZonePosition = GameToken.OUT_OF_BOUND_POSITION;
                IsAtFinish = true;
                Console.WriteLine("llego a la meta color {0}, id {1} y pasos {2} ", AssignedColor,
                    Id, Steps);
            }
        }

        public void MoveInSafeTiles(int roll)
        {
            int oldPosition = SafeZonePosition;
            if (oldPosition > GameToken.WINNING_SAFE_POSITION)
            {
                DebugTokenPos();
                throw new Exception("Nos pasamos de posiciones!!");
            }
            int newPosition = oldPosition + roll;
            if (newPosition < GameToken.WINNING_SAFE_POSITION)
            {
                SafeZonePosition = newPosition;
            }
            else if (newPosition == GameToken.WINNING_SAFE_POSITION)
            {
                SafeZonePosition = GameToken.OUT_OF_BOUND_POSITION;
                IsAtFinish = true;
                Console.WriteLine("llego a la meta color {0}, id {1} y pasos {2} ", AssignedColor,
                    Id, Steps);
            }
            else
            {
                int extraSteps = newPosition - GameToken.WINNING_SAFE_POSITION;
                Console.WriteLine("perdio turno {0}", AssignedColor);
                SafeZonePosition = GameToken.WINNING_SAFE_POSITION - extraSteps;
            }
        }

        public void ResetPosition()
        {
            IsAtStart = true;
            Steps = 0;
            Position = GameToken.OUT_OF_BOUND_POSITION;
        }

    }
}
