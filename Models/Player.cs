﻿using System;
using System.Collections.Generic;
using AutoLudoCLI.Util;
using System.Text;

namespace AutoLudoCLI.Models
{
    public class Player
    {
        public const int NUMBER_OF_TOKENS = 4;

        public TeamColor MyTeamColor { get; }
        public int TokensAtStart { get { return tokenIndexesAtStart.Count; } }
        public int TokensAtFinish { get { return tokenIndexesAtFinish.Count; } }

        Queue<int> tokenIndexesAtStart;
        Queue<int> tokenIndexesAtFinish;
        Queue<int> tokenIndexesInBoard;

        public GameToken[] Tokens { get; }

        public Player(TeamColor teamColor1)
        {
            MyTeamColor = teamColor1;
            tokenIndexesAtStart = new Queue<int>(NUMBER_OF_TOKENS);
            Tokens = new GameToken[NUMBER_OF_TOKENS];
            for (int j = 0; j < NUMBER_OF_TOKENS; j++)
            {
                GameToken token = new GameToken(teamColor1.MyColor, j + 1);
                Tokens[j] = token;
                tokenIndexesAtStart.Enqueue(j);
            }
            tokenIndexesAtFinish = new Queue<int>(NUMBER_OF_TOKENS);
            tokenIndexesInBoard = new Queue<int>(NUMBER_OF_TOKENS);
        }

        public bool HasWon()
        {
            return TokensAtFinish == NUMBER_OF_TOKENS;
        }

        public int RollDice()
        {
            return Dice.Instance.Roll();
        }

        public bool AreTokensInGame()
        {
            return !(TokensAtStart == NUMBER_OF_TOKENS);
        }

        public bool AreTokensInStart()
        {
            return TokensAtStart > 0;
        }

        private void PutTokenInBoard()
        {
            int tokenIndex = tokenIndexesAtStart.Dequeue();
            tokenIndexesInBoard.Enqueue(tokenIndex);
            Tokens[tokenIndex].Position = MyTeamColor.StartPosition;
            Tokens[tokenIndex].IsAtStart = false;
            if (tokenIndexesInBoard.Count > NUMBER_OF_TOKENS)
            {
                DebugPlayer();
                throw new Exception("Demasiadas fichas en la pantalla");
            }
        }

        private void MoveToken(int roll)
        {
            if (tokenIndexesInBoard.Count > 0)
            {
                int tokenIndexToUse = DetermineBestTokenToMove(roll);

                Tokens[tokenIndexToUse].Steps += roll;
                if (CheckTokenInNormalPosition(Tokens[tokenIndexToUse]))
                {
                    MoveInNormalTiles(roll, tokenIndexToUse);
                }
                else
                {
                    MoveInSafeTiles(roll, tokenIndexToUse);
                }

            }
        }

        private int DetermineBestTokenToMove(int roll)
        {
            int tokenIndexToUse = -1;
            if (tokenIndexesInBoard.Count == 1)
            {
                tokenIndexToUse = tokenIndexesInBoard.Peek();
            }
            else
            {
                Dictionary<int, int> tokenIndexPriority = new Dictionary<int, int>(NUMBER_OF_TOKENS);
                foreach (int item in tokenIndexesInBoard)
                {
                    if (IsTokenInBlockade(Tokens[item]) && Dice.Instance.IsStartNumber(roll)
                        && IsThereABlockade(Tokens[item].Position + roll, false))
                    {
                        tokenIndexPriority.Add(item, 7);
                    }
                    else if (Tokens[item].SafeZonePosition != GameToken.OUT_OF_BOUND_POSITION)
                    {
                        tokenIndexPriority.Add(item, AssignPriorityForSafeMovemnts(roll, item));
                    }
                    else
                    {
                        int oldPosition = Tokens[item].Position;
                        int newPosition = oldPosition + roll;
                        if (Board.Instance.IsThereABlockade(oldPosition, newPosition, MyTeamColor))
                        {
                            tokenIndexPriority.Add(item, -1);

                        }
                        else if (IsThereABlockade(newPosition, false))
                        {
                            tokenIndexPriority.Add(item, -1);
                        }
                        else if (Board.Instance.IsPositionOccupiedByAnotherPlayerToken(newPosition, MyTeamColor))
                        {
                            tokenIndexPriority.Add(item, 3);
                        }
                        else if (Tokens[item].Position != MyTeamColor.StartPosition
                            && Tokens[item].Steps > 0
                            && !Board.Instance.IsThereABlockadeInSafeZone(MyTeamColor, newPosition))
                        {
                            tokenIndexPriority.Add(item, 2);
                        }
                        else
                        {
                            tokenIndexPriority.Add(item, 1);
                        }
                    }
                }
                int maxValue = -1;
                int maxSteps = -1;
                foreach (KeyValuePair<int, int> kvp in tokenIndexPriority)
                {
                    if (kvp.Value >= maxValue)
                    {
                        if (kvp.Value == 2)
                        {
                            if (maxSteps == -1)
                            {
                                maxSteps = Tokens[kvp.Key].Steps;
                                maxValue = kvp.Value;
                                tokenIndexToUse = kvp.Key;
                            }
                            else
                            {
                                if (Tokens[kvp.Key].Steps > maxSteps)
                                {
                                    maxValue = kvp.Value;
                                    tokenIndexToUse = kvp.Key;
                                }
                            }
                        }
                        else
                        {
                            maxValue = kvp.Value;
                            tokenIndexToUse = kvp.Key;
                        }

                    }
                }
            }
            if (tokenIndexToUse == -1)
            {
                throw new Exception("No se encontro indice de token a usar");
            }
            return tokenIndexToUse;
        }

        public void SendTokenToStart(GameToken token)
        {
            token.ResetPosition();
            if (token.SafeZonePosition != GameToken.OUT_OF_BOUND_POSITION)
            {
                throw new Exception("Comiendo al que no es");
            }
            List<int> tempArray = new List<int>();
            int tempIndex = tokenIndexesInBoard.Dequeue();
            int steps = 0;
            while (token != Tokens[tempIndex])
            {
                tempArray.Add(tempIndex);
                tempIndex = tokenIndexesInBoard.Dequeue();
                steps++;
                if (steps > 6)
                {
                    DebugPlayer();
                    throw new Exception("Problemas al quitar ficha del tablero");
                }
            }
            if (tokenIndexesInBoard.Count + tempArray.Count > NUMBER_OF_TOKENS)
            {
                throw new Exception("muchos elementos en el juego");
            }
            foreach (int item in tempArray)
            {
                tokenIndexesInBoard.Enqueue(item);
            }
            tokenIndexesAtStart.Enqueue(tempIndex);
            Console.WriteLine("la ficha id {0} del color {1} fue mandada al inicio ", token.Id, token.AssignedColor);
        }

        private int AssignPriorityForSafeMovemnts(int roll, int tokenIndex)
        {
            int oldPosition = Tokens[tokenIndex].SafeZonePosition;
            int newPosition = oldPosition + roll;
            if (newPosition < GameToken.WINNING_SAFE_POSITION)
            {
                return 4;
            }
            else if (newPosition == GameToken.WINNING_SAFE_POSITION)
            {
                return 5;
            }
            else
            {
                return 0;
            }
        }

        private void MoveInSafeTiles(int roll, int tokenIndex)
        {
            Tokens[tokenIndex].MoveInSafeTiles(roll);
            if (Tokens[tokenIndex].IsAtFinish)
            {
                DeQueueTokenFromBoard(tokenIndex);
                tokenIndexesAtFinish.Enqueue(tokenIndex);

            }
        }

        private void DeQueueTokenFromBoard(int tokenIndex)
        {
            List<int> tempArray = new List<int>();
            int tempIndex = tokenIndexesInBoard.Dequeue();
            int steps = 0;
            while (tokenIndex != tempIndex)
            {
                tempArray.Add(tempIndex);
                tempIndex = tokenIndexesInBoard.Dequeue();
                steps++;
                if (steps > 6)
                {
                    DebugPlayer();
                    throw new Exception("Problemas al quitar ficha del tablero");
                }
            }
            if (tokenIndexesInBoard.Count + tempArray.Count > NUMBER_OF_TOKENS)
            {
                throw new Exception("muchos elementos en el juego");
            }
            foreach (int item in tempArray)
            {
                tokenIndexesInBoard.Enqueue(item);
            }
        }

        private void MoveInNormalTiles(int roll, int tokenIndex)
        {
            int oldPosition = Tokens[tokenIndex].Position;
            int newPosition = oldPosition + roll;
            if (newPosition > Board.MAX_BOARD_POSITION)
            {
                newPosition -= (Board.MAX_BOARD_POSITION + 1);
            }
            if (Board.Instance.IsThereABlockade(oldPosition, newPosition, MyTeamColor))
            {
                Console.WriteLine("No se puede mover el personaje por un bloqueo de otro color");
                return;
            }
            if (IsThereABlockade(newPosition, false))
            {
                Console.WriteLine("No se puede mover el personaje porque hay muchas fichas en la misma casilla ");
                return;
            }
            Tokens[tokenIndex].MoveInNormalTiles(roll, MyTeamColor, newPosition);
            if (Tokens[tokenIndex].IsAtFinish)
            {
                DeQueueTokenFromBoard(tokenIndex);
                tokenIndexesAtFinish.Enqueue(tokenIndex);
            }
        }

        private bool CheckTokenInNormalPosition(GameToken gameToken)
        {
            return gameToken.SafeZonePosition == GameToken.OUT_OF_BOUND_POSITION
                && gameToken.Position != GameToken.OUT_OF_BOUND_POSITION;
        }

        public void DebugPlayer()
        {
            Console.WriteLine(" color: {0}, fichas al inicio: {1}, fichas en el tablero: {2}, fichas en la meta {3} ",
                MyTeamColor.MyColor, TokensAtStart, tokenIndexesInBoard.Count, TokensAtFinish);

        }

        public void Play()
        {
            int roll = RollDice();
            Play(roll);
        }

        public void Play(int roll)
        {
            if (!AreTokensInGame())
            {
                if (Dice.Instance.IsStartNumber(roll))
                {
                    PutTokenInBoard();
                }
            }
            else
            {
                if (Dice.Instance.IsStartNumber(roll) && AreTokensInStart())
                {
                    PutTokenInBoard();
                }
                else
                {
                    MoveToken(roll);
                }
            }
            if (Dice.Instance.IsStartNumber(roll))
            {
                Console.WriteLine("A repetir turno!!");
                if (UnitTestDetector.IsRunningInUnitTest)
                {
                    Play(1);
                }
                else
                {
                    Play();
                }

            }

        }

        public bool IsThereABlockade(int position, bool evaluateSafeZone)
        {
            if (position == MyTeamColor.StartPosition && !evaluateSafeZone)
            {
                return false;
            }
            int elementsInPos = 0;
            foreach (int index in tokenIndexesInBoard)
            {
                GameToken token = Tokens[index];
                if (!token.IsAtFinish && !token.IsAtStart && token.Position == position)
                {
                    elementsInPos++;
                }
            }
            return elementsInPos > 1;
        }

        public bool IsTokenInBlockade(GameToken token)
        {
            foreach (int index in tokenIndexesInBoard)
            {
                GameToken anotherToken = Tokens[index];
                if (anotherToken != token)
                {
                    if (!anotherToken.IsAtFinish && !anotherToken.IsAtStart
                        && anotherToken.Position != MyTeamColor.StartPosition
                        && token.Position == anotherToken.Position)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
